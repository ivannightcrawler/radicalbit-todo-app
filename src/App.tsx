import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import './App.css';
import Header from './components/Header';
import UsersList, { IUser } from './components/UsersList';

interface IProps {
  loadData: () => Promise<void>;
  users: IUser[];
}

class App extends React.Component<IProps, {}> {
  public componentDidMount() {
    this.props.loadData();
  }

  public render() {
    return (
      <div className="App">
        <Header />
        <p className="App-intro">
          Welcome to our TODO APP.  
          Please select a user to see their TODOS!
        </p>
        <UsersList />
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return ({
    currentUserId: state.users.currentUserId,
    users: state.users.usersList
  });
};

const mapDispatchToProps = (dispatch: Dispatch ) => ({
  loadData: () => {
    fetch(`https://jsonplaceholder.typicode.com/todos`)
    .then(res => res.json())
    .then(todosJson => dispatch({ type: 'TODOS_UPDATE_SUCCESS', todos: todosJson }))
    .then(todosJson => {
      fetch(`https://jsonplaceholder.typicode.com/users`)
      .then(res => res.json())
      .then(usersJson => dispatch({ type: 'USERS_LIST_UPDATE_SUCCESS', usersList: usersJson }))
      .catch(error => dispatch({ type: 'REQUEST_FAILURE', error }));
    })
    .catch(error => dispatch({ type: 'REQUEST_FAILURE', error }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
 )(App);
