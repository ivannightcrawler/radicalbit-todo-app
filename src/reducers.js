const initialState = {
  todos: [],
  users: {
    currentUserId: undefined,
    usersList: []
  }
}

const todos = (state = initialState.todos, action) => {
  switch (action.type) {
    case 'TODOS_UPDATE_SUCCESS':
      return {...state, todos: action.todos };
    case 'TODOS_TOGGLE_ITEM':
      const updatedTodos = state.todos.map(todo => todo.id === action.id ? {...todo, completed: !todo.completed}: todo);
      return {...state, todos: updatedTodos };
    case 'ADD_TODO_ITEM':
      const newTodos = [action.todo, ...state.todos];
      return { ...state, todos: newTodos };
    default:
      return state
  }
}

const users = (state = initialState.users, action) => {
  switch (action.type) {
    case 'UPDATE_ACTIVE_USER':
      return {
        ...state,
        currentUserId: action.userId
      }
    case 'USERS_LIST_UPDATE_SUCCESS':
      return {
        ...state, 
        usersList: action.usersList
      };
    default:
      return state
  }
}

export default {
  todos,
  users
};