import { Button, Typography} from 'material-ui';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import * as React from 'react';
import { connect } from 'react-redux';
import Header from './Header';
import './UserDetailPage.css';
import { IUser } from './UsersList';

interface IProps {
  user: IUser;
  match: {
    params: { id: string }
  }
}

class UserDetailPage extends React.Component<IProps, {}> {
  public render() {
    return (
      <div className="UserDetailPage">
        <Header />
        <Card>
          <CardMedia
            image="http://via.placeholder.com/350x150?text=User+Needs+An+Image!"
            title={this.props.user ? this.props.user.username : '...loading info'}
          />
          <CardContent>
            <Typography gutterBottom={true} variant="headline" component="h2">
            {this.props.user ? this.props.user.name : '...loading info'} ({this.props.user ? this.props.user.username : '...loading info'})
            </Typography>
            <Typography component="p">
            {this.props.user ? this.props.user.phone : '...loading info'}
            </Typography>
            <Typography component="p">
            {this.props.user ? this.props.user.website : '...loading info'}
            </Typography>
          </CardContent>
          <CardActions>
            <Button size="small" color="primary" disabled={true}>
              Share
            </Button>
            <Button size="small" color="primary" disabled={true}>
              Learn More
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state: any, ownProps: IProps) => {
  return ({
    user: state.users.usersList.filter((user: IUser) => Number(user.id) === Number(ownProps.match.params.id))[0]
  })
};

export default connect(
  mapStateToProps,
  null
 )(UserDetailPage);
