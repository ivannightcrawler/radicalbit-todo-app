import { Button, Icon } from 'material-ui';
import List, { ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, } from 'material-ui/List';
import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { push } from 'react-router-redux';
import './UsersList.css';

interface IProps {
  users: IUser[];
  currentUserId: number;
  updateUsers: () => Promise<void>;
  updateActiveUser: (id: number) => Promise<void>;
}

export interface IUser {
  id: number;
  name: string;
  username: string;
  email: string;
  phone: string;
  website: string;
}

class UsersList extends React.Component<IProps, {}> {
  public componentDidMount() {
    this.props.updateUsers();
  }

  public selectUser = (id: number) => () => {
    this.props.updateActiveUser(id);
  }

  public render() {
    return (
      <List component="nav">
        {this.props.users.length > 0 && this.props.users.map((user:IUser, i) => 
        <ListItem 
          key={user.id} 
          button={true} 
          onClick={this.selectUser(user.id)} 
          className={this.props.currentUserId === user.id ? 'active-user' : ''} 
        >
          <ListItemIcon>
          <Icon>account_circle</Icon>
          </ListItemIcon>
          <ListItemText inset={true} primary={user.name} />
          <ListItemSecondaryAction>
            <Link to={`/users/${user.id}`}>
              <Button aria-label="User Details">
                See User Details <Icon>assignment_ind</Icon>
              </Button>
            </Link>
          </ListItemSecondaryAction>
        </ListItem>
      )}
      </List>
    );
  }
}

const mapStateToProps = (state: any) => ({
  currentUserId: state.users.currentUserId || -1,
  users: state.users.usersList
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  updateActiveUser: (id: number) => {
    dispatch({ type: 'UPDATE_ACTIVE_USER', userId: id });
    dispatch(push(`/todos`));
  },
  updateUsers: () => {
    fetch(`https://jsonplaceholder.typicode.com/users`)
    .then(res => res.json())
    .then(json => dispatch({ type: 'USERS_LIST_UPDATE_SUCCESS', usersList: json }))
    .catch(error => dispatch({ type: 'REQUEST_FAILURE', error }));
  }
});

export default connect<Partial<IProps>>(
  mapStateToProps,
  mapDispatchToProps
 )(UsersList);