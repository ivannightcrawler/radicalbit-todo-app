import { Button, Checkbox, Icon } from 'material-ui';
import List, { ListItem, ListItemText } from 'material-ui/List';
import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import Dialog from './AddListDialog';
import Header from './Header';
import './TodosPage.css';


interface IProps {
  todos: ITodo[];
  userId: number;
  updateTodos: () => Promise<void>;
  toggleTodo: (id: number) => Promise<void>;
}

export interface ITodo {
  completed: boolean;
  id: number;
  title: string;
  userId: number;
}

class TodosPage extends React.Component<IProps, {}> {
  public state = {
    isDialogOpen: false
  }

  public componentDidMount() {
    this.props.updateTodos();
  }

  public handleToggle = (todoId: number) => () => {
    this.props.toggleTodo(todoId);
  };

  public openDialog = () => {
    this.setState({
      isDialogOpen: true
    });
  }

  public closeDialog = () => {
    this.setState({
      isDialogOpen: false
    });
  }

  get todos() {
    return this.props.userId
    ? this.props.todos.filter(todo => todo.userId === this.props.userId)
    : this.props.todos
  }

  public render() {
    return (
      <div>
        <Header />
        <List>
        {
          this.todos.map((todo: ITodo) => 
            <ListItem
            key={todo.id}
            role={undefined}
            dense={true}
            button={true}
            onClick={this.handleToggle(todo.id)}
          >
          <Checkbox
            checked={todo.completed}
            tabIndex={-1}
            disableRipple={true}
          />
          <ListItemText primary={todo.title} />
        </ListItem>)
        }
        </List>
        <Button variant="fab" color="secondary" className="add-btn" onClick={this.openDialog} disabled={this.props.userId === undefined}>
          <Icon>add</Icon>
        </Button>
        <Dialog open={this.state.isDialogOpen} handleClose={this.closeDialog} /> 
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  todos: state.todos.todos || [],
  userId: state.users.currentUserId
});

const mapDispatchToProps = (dispatch: Dispatch ) => ({
  toggleTodo: (id: number) => dispatch({ type: 'TODOS_TOGGLE_ITEM', id }),
  updateTodos: () => {
    fetch(`https://jsonplaceholder.typicode.com/todos`)
    .then(res => res.json())
    .then(json => dispatch({ type: 'TODOS_UPDATE_SUCCESS', todos: json }))
    .catch(error => dispatch({ type: 'REQUEST_FAILURE', error }));
  },
});

export default connect<Partial<IProps>>(
  mapStateToProps,
  mapDispatchToProps
 )(TodosPage);