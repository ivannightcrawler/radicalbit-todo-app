import MenuIcon from '@material-ui/icons/Menu';
import { AppBar, Button, IconButton, List, SwipeableDrawer, Toolbar, Typography } from 'material-ui';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './Header.css';
import { IUser } from './UsersList';

interface IState {
  open: boolean;
}

interface IProps {
  username: string;
}

class Header extends React.Component<IProps, IState> {
  public state = {
    open: false
  }

  public toggleDrawer = () => {
    this.setState({
      open: !this.state.open,
    });
  }

  public render() {
    return (
      <header className="Header">      
        <AppBar>
          <Toolbar>
            <IconButton className="mobile-menu" color="inherit" aria-label="Menu" onClick={this.toggleDrawer}>
              <MenuIcon />
            </IconButton>
            <Link to="/">
              <Typography variant="title" color="inherit">
                TODO APP
              </Typography>
            </Link>
            <div className="Header-buttons">
              <Link to="/users"><Button color="inherit">Change User</Button></Link>
              <Link to="/todos"><Button color="inherit">{this.props.username.length > 0 ? `${this.props.username}'s `: `` }Todos</Button></Link>
            </div>
            <SwipeableDrawer open={this.state.open} onClose={this.toggleDrawer} onOpen={this.toggleDrawer}>
              <div
                tabIndex={0}
                role="button"
                onClick={this.toggleDrawer}
                onKeyDown={this.toggleDrawer}
              >
              <List className="Header-mobile-link"><Link to="/"><Button color="inherit">Home</Button></Link></List>
              <List className="Header-mobile-link"><Link to="/users"><Button color="inherit">Change User</Button></Link></List>
              <List className="Header-mobile-link"><Link to="/todos"><Button color="inherit">{this.props.username.length > 0 ? `${this.props.username}'s `: `` }Todos</Button></Link></List>
              </div>
            </SwipeableDrawer>
          </Toolbar>
        </AppBar>
      </header>
    );
  }
}

const mapStateToProps = (state: any) => {
  const currentUser = state.users.usersList.filter((user: IUser) => user.id === state.users.currentUserId);
  return ({
    username: currentUser[0] ? currentUser[0].username : ''
  });
};

export default connect(
  mapStateToProps,
  null
 )(Header);
