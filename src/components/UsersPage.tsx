import * as React from 'react';
import Header from './Header';
import { IUser } from './UsersList';
import UsersList from './UsersList';
import './UsersPage.css';

interface IProps {
  users: IUser[];
}

export default class UsersPage extends React.Component<IProps, {}> {
  public render() {
    return (
      <div className="UsersPage">
        <Header />
        <p className="UsersPage-intro">
          Please select a user to see their TODOS
        </p>
        <UsersList />
      </div>
    );
  }
}
