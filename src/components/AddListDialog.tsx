import { Button, Dialog, DialogActions, DialogContentText, DialogTitle, TextField } from 'material-ui';
import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import './AddListDialog.css';
import { ITodo } from './TodosPage';

interface IProps extends IOwnProps {
  open: boolean;  
  handleClose: () => void;
}

interface IOwnProps {
  userId: number;
  addTodo: (todo: ITodo) => Promise<void>;
}

class TodosPage extends React.Component<IProps, {}> {
  public handleSubmit = (event: React.FormEvent<HTMLFormElement>) =>  {
    event.preventDefault();
    
    const data = new FormData(event.target as HTMLFormElement);
    const title = data.get('title') as string;

    if (title !== null && title.length > 0) {
      const todo = {
        completed: false,
        id: Math.floor((Math.random() * 100) + 50), // fake key
        title,
        userId: this.props.userId
      }
      
      this.props.addTodo(todo);
    }
    // TODO: Qui si può aggiungere una notifica se il todo è vuoto.    

    // chiudere la modal
    this.props.handleClose();
  }

  public render() {
    return (
      <div>
        <Dialog onClose={this.props.handleClose} aria-labelledby="simple-dialog-title" open={this.props.open}>
          <form onSubmit={this.handleSubmit}>
            <DialogTitle id="simple-dialog-title">Add todo</DialogTitle>
            <div className="Dialog-spacer">
              <DialogContentText>
                Vestibulum tempor, libero sed porta consequat, nunc mauris condimentum erat, consectetur tristique lacus massa nec lorem.
              </DialogContentText>
              <TextField
                  autoFocus={true}
                  margin="dense"
                  id="title"
                  name="title"
                  label="To Do"
                  type="TODO:"
                  fullWidth={true}
                />
            </div>
            <DialogActions>
              <Button onClick={this.props.handleClose} color="primary">
                  Cancel
              </Button>
              <Button color="primary" type="submit">
                  Add
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  userId: state.users.currentUserId
});

const mapDispatchToProps = (dispatch: Dispatch ) => ({
  addTodo: (todo: ITodo) => dispatch({ type: 'ADD_TODO_ITEM', todo}),
  updateTodos: () => {
    fetch(`https://jsonplaceholder.typicode.com/todos`)
    .then(res => res.json())
    .then(json => dispatch({ type: 'TODOS_UPDATE_SUCCESS', todos: json }))
    .catch(error => dispatch({ type: 'REQUEST_FAILURE', error }));
  },
});

export default connect<Partial<IOwnProps>>(
  mapStateToProps,
  mapDispatchToProps
 )(TodosPage);