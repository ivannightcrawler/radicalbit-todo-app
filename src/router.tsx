import * as React from 'react';
import { Redirect, Route  } from 'react-router';
import App from './App';
import TodosPage from './components/TodosPage';
import UserDetailPage from './components/UserDetailPage';
import UsersPage from './components/UsersPage';

export default () => <div>
  <Redirect from='*' to='/' />
  <Route exact={true} path="/" component={App}/>
  <Route path="/todos" component={TodosPage}/>
  <Route exact={true} path="/users" component={UsersPage}/>
  <Route exact={true} path="/users/:id" component={UserDetailPage}/>
</div>